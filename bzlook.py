__module_name__ = 'bzlook'
__module_description__ = 'Look up Bugzilla info when a link is posted in chat'
__module_version__ = '0.3.2'

import xchat, re
# https://pypi.org/project/python-bugzilla/
import bugzilla

# Generate API key at https://bugzilla.redhat.com/userprefs.cgi?tab=apikey
BZ_API_KEY='my_api_key'
BUGZILLA_API_URL = 'https://bugzilla.redhat.com/rest/'
BUG_PATTERN = re.compile(r'bugzilla\.redhat\.com\/[show_bug\.cgi\?id=]*([0-9]+)',re.IGNORECASE)

xchat.prnt('%(name)s, version %(version)s' % {'name': __module_name__,  'version': __module_version__})  

def check_msg(word, word_eol, userdata):
    if 'bugzilla.redhat.com' in word_eol[1].lower():
        try:
            bug_id = BUG_PATTERN.search(word_eol[1]).group(1)
        except:
            # if we don't find 'bugzilla.redhat.com/<id>' 
            # or 'bugzilla.redhat.com/show_bug.cgi?id=<id>' then move on
            return xchat.EAT_NONE

        try:
            bug = get_bz_info(bug_id)
        except Exception as e:
            xchat.command('query @bzlook')
            tab = xchat.find_context(channel='@bzlook')
            tab.prnt(e)
            xchat.emit_print('Channel Message', 'bzlook', 'Could not look up BZ %s' % bug_id)
            return xchat.EAT_NONE

        bz_status = '[%s] %s -- %s %s -- %s %s %s' % ( bug['id'],
                                                       bug['summary'],
                                                       bug['status'],
                                                       bug['resolution'],
                                                       bug['product'],
                                                       bug['version'],
                                                       bug['component'])

        if bug['status'] != 'CLOSED':
            bz_status += '\nPrio: %s | Sev: %s | Assigned to: %s' % (get_bz_priority(bug),
                                                                     get_bz_severity(bug),
                                                                     get_bz_assigned_to(bug))
    
        xchat.emit_print('Channel Message', 'bzlook', bz_status)

    return xchat.EAT_NONE

def get_bz_info(bz_num):
    try:
        bzapi = bugzilla.Bugzilla(url=BUGZILLA_API_URL, api_key=BZ_API_KEY, use_creds=False)
        bug = bzapi.getbug(bz_num).get_raw_data()
    except:
        raise

    return bug

def get_bz_severity(bug):
    # https://modern.ircdocs.horse/formatting.html#color
    severity = { 'urgent' : '\0034Urgent\003',
                 'high' : '\0037High\003',
                 'medium' : '\0038Medium\003',
                 'low' : '\00310Low\003',
                 'unspecified' : '\00315unspecified\003'}

    return severity[bug['severity']]

def get_bz_priority(bug):
    # https://modern.ircdocs.horse/formatting.html#color
    priority = { 'urgent' : '\0034Urgent\003',
                 'high' : '\0037High\003',
                 'medium' : '\00310Medium\003',
                 'low' : '\00314Low\003',
                 'unspecified' : '\00315unspecified\003'}

    return priority[bug['priority']]

def get_bz_assigned_to(bug):
    assigned_to_name = bug['assigned_to_detail']['real_name']
    assigned_to_email = bug['assigned_to_detail']['name']

    return '%s %s' % (assigned_to_name, assigned_to_email)

xchat.hook_print("Channel Message", check_msg)
